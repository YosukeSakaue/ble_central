// This is a JavaScript file
'use strict';

//call main function
function index(){
    app.bindEvents();   //add event 
    app.showMainPage();   //show Main page 
}

const UUID = new Uuid("honban");   //UUID constracter.

// main app object
var app = {
    //add events listener
    bindEvents: function() {
        refreshButton.addEventListener('click', this.refreshDeviceList, false);    //refreshボタンのタッチイベント登録
        testButton.addEventListener('click', this.disconnect, false);        //disconnectボタンのタッチイベント登録
    },
    //view is list
    refreshDeviceList: function() {
        deviceList.innerHTML = 'List messege...<br/>'; // empties the list

//----------------------------------------------------------------------------------
//      callbackの処理順序のテスト
        var Func1 = function(){
            app.onAddListItem(debugDate() + ': Func1 call.');
        }
        var Func2 = function(){
            app.onAddListItem(debugDate() + ': Func2 call.');
        }
        var Func3 = function(callback){
            app.onAddListItem(debugDate() + ': Func3 call.');
            setTimeout(callback,0);
        }
        var Func4 = function(){
            app.onAddListItem(debugDate() + ': Func4 call.');
        }
        Func1();
        //throwが発生しうる関数をtryで呼び出す。処理途中でthrowが発生するとcatchが実行される。その後、処理は継続する。
        try{
            throw (new Error('test error!!'));
        }
        //catchすると例外処理をして処理を継続する
        catch(e){
            console.warn(e.name);        //Errorを返す
            console.warn(e.message);     //test error!!を返す
        }

        Func3(Func2);
        for (var i=0; i < 2; i++){
            Func4();
        }

// --------------------------------------------------------------------------------
        //配列の違いを検証：Arrayオブジェクトと[]リテラル
        //結論は、Arrayは事前にnewが必要。リテラルは直接入れられる。簡素化か可読性かかの違いか？
        //参考：http://qiita.com/takeharu/items/d75f96f81ff83680013f 
        var listarray = new Array();
        var listarray2 = [];

        listarray = ["a","aaa"];
        // listarray2 = ["b","c"];
        console.log("listarray : " + listarray.length + " : " + listarray);
        console.log("listarray2 : " + listarray2.length + " : " + listarray2);

        listarray[0] = "bbb";   //index指定で変更
        listarray2.push("zzz"); //配列の末尾に追加
        listarray.unshift("ccc");   //配列の先頭に追加
        console.log("listarray : " + listarray.length + " : " + listarray);
        console.log("listarray2 : " + listarray2.length + " : " + listarray2);
        // var html = '<b>' + device.name + '</b><br/>' +
        //         'RSSI: ' + device.rssi + '&nbsp;|&nbsp;' +
        //         device.id;
        // app.onAddListItem(html)
// --------------------------------------------------------------------------------
        //ble central unit test -- Funcrion lockingBoxTransfer
        var ownAccount = new OwnAccount("UnitTest", "0xabcdefghijklmnopqrstuvwxyz" ,"123456789a123456789b123456789c123456789d123456789e123456789f123456789g12345");

        lockingBoxTransfer("success", ownAccount, "01120", function(success, failur){
            if(failur){
                console.error("lokingBoxTransfer call Error, " + failur);
            } else {
                console.log("lockingBoxTransfer call success, " + success);
            }
        });
    },
    // add list item(li)
    onAddListItem: function(html){
        var listItem = document.createElement('li')
        listItem.innerHTML = html;
        deviceList.appendChild(listItem);
    },
    //disconnect function
    disconnect: function(event) {
        //stop interval
        var intervalId = event.target.dataset.intervalId;
        clearInterval(intervalId);
        
        //ble disconnect
        console.log(debugDate() + ": disconnect:" + event);
        var deviceId = event.target.dataset.deviceId;
        ble.disconnect(deviceId, app.showMainPage, app.onError);
    },
    //show main page site.
    showMainPage: function() {
        mainPage.hidden = false;
        subPage.hidden = true;
    },
    // show sub page site. 
    showsubPage: function() {
        mainPage.hidden = true;
        subPage.hidden = false;
    },
    //Error メッセージ
    onError: function(reason) {
        console.log(debugDate() + ": ERROR: " + reason);
    }
};

//debug Timestamp
function debugDate(){
    return new Date().toLocaleString();
}
