// This is a JavaScript file
'use strict';

//debug Timestamp
function debugDateMsg(msg){
    console.log(new Date().toLocaleString() + "-> " + msg);
}

const UUID = new Uuid("test");  //UUID constracter

// main app object
var app = {
    //initialize
    initialize: function() {
        this.bindEvents();
        detailPage.hidden = true;
    },
    //イベントリスナー登録
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);            //cordova deviceready判定
        refreshButton.addEventListener('touchstart', this.refreshDeviceList, false);    //refreshボタンのタッチイベント登録
        disconnectButton.addEventListener('touchstart', this.disconnect, false);        //disconnectボタンのタッチイベント登録
        deviceList.addEventListener('touchstart', this.connect, false);                 //デバイスリストのタッチイベント登録
    },
    //cordove deviceready event function
    onDeviceReady: function() {
        //bluetooth status check
        app.onIsEnabled();
        app.refreshDeviceList();
    },
    //ble.scan && output listitem.
    refreshDeviceList: function() {
        deviceList.innerHTML = ''; // empties the list
        // scan for all devices
        ble.scan([], 5, app.onDiscoverDevice, app.onError);
    },
    //scan deviceのリスト表示
    onDiscoverDevice: function(device) {
        var listItem = document.createElement('li'),
            html = '<b>' + device.name + '</b><br/>' +
                'RSSI: ' + device.rssi + '&nbsp;|&nbsp;' +
                device.id

        listItem.dataset.deviceId = device.id;
        listItem.dataset.deviceName = device.name;
        listItem.innerHTML = html;
        deviceList.appendChild(listItem);
        debugDateMsg("device_name: " + device.name);
        debugDateMsg("device_rssi: " + device.rssi);
        debugDateMsg("device_Id: " + device.id);
        debugDateMsg("device_advertising: " + bytesToString(device.advertising));
    },
    //connect event function
    connect: function(e) {
        var deviceId = e.target.dataset.deviceId,
            deviceName = e.target.dataset.deviceName;
        //touch event check
        if (e.target.nodeName != "LI") {
            debugDateMsg("touch event unmatch, " + e.target.nodeName );
            return;
        }
        if (!/BOX/.test(deviceName)){
            debugDateMsg("touch event unmatch deviceName, " + deviceName);
            return;
        }
        debugDateMsg("touch connect event!");
//
        //ble central unit test -- Funcrion lockingBoxTransfer
        var ownAccount = new OwnAccount("UnitTest", "0xabcdefghijklmnopqrstuvwxyz" ,"123456789a123456789b123456789c123456789d123456789e123456789f123456789g12345");

        lockingBoxTransfer(deviceId, ownAccount, "01120", function(success, failur)
        {
            if(failur){
                console.warn("lokingBoxTransfer callback failur, " + failur);
            } else {
                console.log("lockingBoxTransfer callback success, " + success);
            }
        });
    },
    //コネクションしているデバイスのRSSI値の取得
    onReadRSSI: function(deviceID) {
        var onReadRSSI = function(intRSSI){
            console.log(debugDateMsg() + ": readRSSI value= " + intRSSI );                    
        };
        console.log(debugDateMsg() + ": readRSSI is invoked");
        ble.readRSSI(deviceID, onReadRSSI, app.onError);
    },
    //コネクション状態の確認
    onIsConnected: function(deviceID) {
        var onSuccess = function(returnId){
            console.log(debugDateMsg() + ": isConnected success= " + returnId );                    
        };
        console.log(debugDateMsg() + ": isConnected is invoked");
        ble.isConnected(deviceID, onSuccess, app.onError);
    },
    //Bluetootの有効有無を返却
    onIsEnabled: function() {
        debugDateMsg("isEnabled is invoked.");
        ble.isEnabled(function(success){
                debugDateMsg("isEnabled success, " + success);
        }, app.onError);
    },
    //Bluetooth状態通知のstop
    stopStateNotify: function(){
        console.log(debugDateMsg() + ": stopStateNotify...");
        ble.stopStateNotifications(function(){
            console.log(debugDateMsg() + ": stopStateNotify success!!");
            },app.onError);
    },
    //disconnect function
    disconnect: function(event) {
        //stop state Notifications
        app.stopStateNotify();
        
        //stop interval
        var intervalId = event.target.dataset.intervalId;
        clearInterval(intervalId);
        
        //ble disconnect
        console.log(debugDateMsg() + ": disconnect:" + event);
        var deviceId = event.target.dataset.deviceId;
        ble.disconnect(deviceId, app.showMainPage, app.onError);
    },
    //
    showMainPage: function() {
        mainPage.hidden = false;
        detailPage.hidden = true;
    },
    showDetailPage: function() {
        mainPage.hidden = true;
        detailPage.hidden = false;
    },
    //Error メッセージ
    onError: function(reason) {
        debugDateMsg("ERROR, " + reason);
    }
};
