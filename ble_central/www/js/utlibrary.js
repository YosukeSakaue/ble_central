//This JS Library Unit testing.
var NotifyIntervalid;   //Nortification Interval id

var ble = {
    //stopNotification function
    // ble.stopNotification(device_id, service_uuid, characteristic_uuid, success, failure);
    stopNotification: function(device_id, service_uuid, characteristic_uuid, success, failure)
    {
        //parameter check
        if(typeof(device_id) === 'undefined' || device_id ==''){
            throw new Error(debugMsg() + "UTLogic device_id is undefined");
        };
        if(typeof(service_uuid) === 'undefined' || service_uuid ==''){
            throw new Error(debugMsg() + "UTLogic service_uuid is undefined");
        };
        if(typeof(characteristic_uuid) === 'undefined' || characteristic_uuid ==''){
            throw new Error(debugMsg() + "UTLogic characteristic_uuid is undefined");
        };
        
        if(NotifyIntervalid === 'undefined'){
            failure("ble.stopNotification error!");
        } else {
            clearInterval(NotifyIntervalid);
            success("OK");
        };
    },
    //disconnect function
    // ble.disconnect(device_id, [success], [failure]);
    disconnect: function(device_id, success, failure)
    {
        //parameter check
        if(typeof(device_id) === 'undefined' || device_id ==''){
            throw new Error(debugMsg() + "UTLogic device_id is undefined");
        };
        if(/success/.test(device_id)){
            success("OK");
        } else {
            failure("NG");
        };
    },
    //connect function
    // ble.connect(device_id, connectSuccess, connectFailure);
    connect: function(device_id, connectSuccess, connectFailure)
    {
        //parameter check
        if(typeof(device_id) === 'undefined' || device_id ==''){
            throw new Error(debugMsg() + "UTLogic device_id is undefined");
        };
        if(/success/.test(device_id)){
            connectSuccess("OK");
        } else {
            connectFailure("NG");
        };
    },
    //startNotification function
    // ble.startNotification(device_id, service_uuid, characteristic_uuid, success, failure);
    startNotification: function(device_id, service_uuid, characteristic_uuid, success, failure)
    {
        //parameter check
        if(typeof(device_id) === 'undefined' || device_id ==''){
            throw new Error(debugMsg() + "UTLogic device_id is undefined");
        };
        if(typeof(service_uuid) === 'undefined' || service_uuid ==''){
            throw new Error(debugMsg() + "UTLogic service_uuid is undefined");
        };
        if(typeof(characteristic_uuid) === 'undefined' || characteristic_uuid ==''){
            throw new Error(debugMsg() + "UTLogic characteristic_uuid is undefined");
        };

        const data = "Notify DUMMY Data.";
        var timer = Math.floor(Math.random()*11)*1000;

        console.log(debugMsg() + "UTLogic ble.startNotification Request.");
        console.log(debugMsg() + "UTLogic setTimeout : " + timer);

        if(/success/.test(device_id)){
            NotifyIntervalid = setInterval(function(){
                success(stringToBytes(data));
            },timer);
        } else {
            setTimeout(function() {
                failure("UTLogic ble.statNotification Error.");
            }, timer);
        };
    },
    //write function
    //  ble.write(device_id, service_uuid, characteristic_uuid, data, success, failure)
    //  device_id = "/success/" -> success, unmatch failure
    write: function(device_id, service_uuid, characteristic_uuid, data, success, failure)
    {
        //parameter check
        if(typeof(device_id) === 'undefined' || device_id ==''){
            throw new Error(debugMsg() + "UTLogic device_id is undefined");
        };
        if(typeof(service_uuid) === 'undefined' || service_uuid ==''){
            throw new Error(debugMsg() + "UTLogic service_uuid is undefined");
        };
        if(typeof(characteristic_uuid) === 'undefined' || characteristic_uuid ==''){
            throw new Error(debugMsg() + "UTLogic characteristic_uuid is undefined");
        };
        if(typeof(data) === 'undefined' || data ==''){
            failure("data is undefined");
            return;
        };

        console.log(debugMsg() + "UTLogic ble.write request.");
        console.log(debugMsg() + "UTLogic Request data : " + bytesToString(data));
        if(/success/.test(device_id)){
            setTimeout(function() {
                success("OK");
            }, 1000);
        } else {
            setTimeout(function() {
                failure("NG");
            }, 1000);
        };
    },
    //scan function
    scan: function(UUID, timer, resolve, reject){
            if(!Array.isArray(UUID)){
                reject("UUID param error!");
                return;
            };
            if(!isFinite(timer)){
                reject("Timer param error!");
                return;
            };
            var device = {advertising: "", name: "", id: "", rssi: 0};
            if(timer > 1){
                setTimeout(function(){
                    device.advertising = "CloseBox";
                    device.name = "BOX01";
                    device.id = "33CF46E8-A021-46A7-A83D-8C7CFC2E8BC2";
                    device.rssi = -60.2;
                    resolve(device);
                },1000);
            };
            if(timer > 2){
                setTimeout(function(){
                    device.advertising = "CloseBox";
                    device.name = "BOX02";
                    device.id = "33CF46E8-A021-46A7-A83D-8C7CFC2E8BC2";
                    device.rssi = -40.1;
                    resolve(device);
                },2000);
            };
            if(timer > 3){
                setTimeout(function(){
                    device.advertising = "OpenBox";
                    device.name = "BOX03";
                    device.id = "33CF46E8-A021-46A7-A83D-8C7CFC2E8BC2";
                    device.rssi = -53.2;
                    resolve(device);
                },3000);
            };
            if(timer > 4){
                setTimeout(function(){
                    device.advertising = "OpenBox";
                    device.name = "BOX04";
                    device.id = "33CF46E8-A021-46A7-A83D-8C7CFC2E8BC2";
                    device.rssi = -10.2;
                    resolve(device);
                },4000);
            };
            if(timer > 5){
                setTimeout(function(){
                    device.advertising = "CloseBox";
                    device.name = "BOX05";
                    device.id = "33CF46E8-A021-46A7-A83D-8C7CFC2E8BC2";
                    device.rssi = -23.0;
                    resolve(device);
                },5000);
            };
            if(timer > 6){
                setTimeout(function(){
                    reject("UTLogic timeout error.");
                },6000);
            };
    }
};

// ASCII only
function stringToBytes(string) {
   var array = new Uint8Array(string.length);
   for (var i = 0, l = string.length; i < l; i++) {
       array[i] = string.charCodeAt(i);
    }
    return array.buffer;
}
// ASCII only
function bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}


// //ble.startNotification test logic...
// ble.startNotification("success", "uuid_1", "uuid_2", 
//     function(data){
//         console.log(console.log() + ": notify success : " + bytesToString(data));
//     },
//     function(error){
//         console.log(debugMsg() + ": notify failure : "  + error);
//     });

// // ble.scan test logic...
// ble.scan([],5,function(device){
//         console.log("advertising:" + device.advertising);
//         console.log("name: " + device.name);
//         console.log("id: " + device.id);
//         console.log("rssi: " + device.rssi);
// },function(error){
//     console.log("ERROR: " + error);
// });

// //ble.write unit test logic...
// var testdata = stringToBytes("TEST DATA");
// ble.write("success", "uuid_1", "uuid_2", testdata, 
//     function(success){
//         console.log("unit test callback success : " + success);
//     }, 
//     function(failure){
//         console.log("unit test callback failure : " + failure);
// });

//int8Arrayのお試し
// var onDate = function(buffer){
//     onDate.notifydate = bytesToString(buffer);
// };

// onDate([97]);
// console.log(onDate.notifydate);
// // ASCII only
// function stringToBytes(string) {
//    var array = new Uint8Array(string.length);
//    for (var i = 0, l = string.length; i < l; i++) {
//        array[i] = string.charCodeAt(i);
//     }
//     return array.buffer;
// }

// // ASCII only
// function bytesToString(buffer) {
//     return String.fromCharCode.apply(null, new Uint8Array(buffer));
// }
