//ble central (Bluetooth Low Energy (BLE) Central Plugin for Apache Cordova)
//  note: Unit test using utlibrary.js library. 

//RaspberryPi Service & characteristic UUIS
//Constructer function for UUID Definitions
//  note: Uuid needs to be declared as new in the caller.
function Uuid(mode){
    if( mode == "honban"){
        //UUID Definitions for production.
        // Advertise service UUID
        this.AdvertiseService = "33CF46E8-A021-46A7-A83D-8C7CFC2E8BC2";
        // Get box's list service UUID, Read a box account form characteristic.
        this.BoxIdService = "9D0957D7-8F90-434F-BD9C-23BF1AFCEAD4";
        this.BoxIdCharacteristic = "CE67A8AE-9F98-481E-8EE7-CF9D450E1677";
        // lock of the box service UUID, Write a delivery account and signature form characteristic.
        this.KeylockService = "684FD56A-D2EB-443E-B1BC-1948CF8F1489";
        this.LockBoxCharacteristic = "D4719680-E5B4-42DD-A6A2-B03A3BE77FEE";
        this.LockComsign1Characteristic ="2AAAD579-3C42-4112-BB66-D78BA5CB79AA";
        this.LockComsign2Characteristic ="5F12B3F9-574B-4A6C-B154-01CC1F83D408";
        this.LockComsign3Characteristic ="6805E8CD-A5EF-47E0-839B-06674F436903";
        this.LockComsign4Characteristic ="A3924879-1138-42CD-9A83-821D9B83A06E";
        this.LockResultCharacteristic ="9E262F0D-AE29-4D39-BF40-B99611A4EBBC";
        // unlock of the box service UUID, Write a customer account and signature from characteristic. 
        this.KeyopenService = "5496BC13-903A-4800-983D-E426090DC025";
        this.OpenBoxCharacteristic = "0E726901-A958-48CF-97B6-1E91626BAC5D";
        this.OpenUseradressCharacteristic ="2E3DC3C6-40F0-4501-9909-7261766FA448";
        this.OpenUsersign1Characteristic ="B7D1139A-A2A2-4557-B89F-F0587258A058";
        this.OpenUsersign2Characteristic ="6F10182E-B158-44F8-ACC7-53BA8E9DF8CC";
        this.OpenUsersign3Characteristic ="9FF47331-6937-4129-B3CE-AA0E2C133E2E";
        this.OpenUsersign4Characteristic ="9BF6A380-DDFC-4BD2-8BB3-90BF969EB262";
        this.OpenResultCharacteristic ="C89BC61A-A6DF-459C-8717-598EDEA74395";
        // Get box status service UUID, Read a box status code form characteristic.
        this.KeyinfoService = "15066256-522D-4FC6-B300-258977F7888E";
        this.KeyStatusCharacteristic = "432C8478-8A81-45CE-84B1-2AB53400E065";
    } else {
        //UUID Definitions for debugging.(use abbreviaed name)
        // Advertise service UUID
        this.AdvertiseService = "A001";
        // Get box's list service UUID, Read a box account form characteristic.
        this.BoxIdService = "B001";
        this.BoxIdCharacteristic = "BC01";
        // lock of the box service UUID, Write a delivery account and signature form characteristic.
        this.KeylockService = "C001";
        this.LockBoxCharacteristic = "CC01";
        this.LockComsign1Characteristic ="CC11";
        this.LockComsign2Characteristic ="CC12";
        this.LockComsign3Characteristic ="CC13";
        this.LockComsign4Characteristic ="CC14";
        this.LockResultCharacteristic ="CC21";
        // unlock of the box service UUID, Write a customer account and signature from characteristic. 
        this.KeyopenService = "D001";
        this.OpenBoxCharacteristic = "DC01";
        this.OpenUseradressCharacteristic ="DC11";
        this.OpenUsersign1Characteristic ="DC21";
        this.OpenUsersign2Characteristic ="DC22";
        this.OpenUsersign3Characteristic ="DC23";
        this.OpenUsersign4Characteristic ="DC24";
        this.OpenResultCharacteristic ="DC31";
        // Get box status service UUID, Read a box status code form characteristic.
        this.KeyinfoService = "E001";
        this.KeyStatusCharacteristic = "EC01";        
    }
}

//owner Account interface. (Constructor)
//  note: ownAccount needs to be declared as new in the caller.
var OwnAccount = function(ownName, ownAddress, ownSign){
    if(arguments.length != 3){
        throw new Error("ownAccount Constructer error.");
    }
    this.ownName = ownName;
    this.ownAddress = ownAddress;
    this.ownSign = ownSign;
};

//----------------------------------------------------------------
//box scan and list ganerations
//    args : ownerflug = 1 [delivery], 2[customer]
var scanBoxlist = function scanBoxlist(ownerflug, callback) {
    var boxlist = new Array();  //TODO [] idiom?
    //name : "BOX~" , UUID : AdvertiseService
    //Advertising:"OpenBox" or "CloseBox"
    ble.scan([UUID.AdvertiseService], 5, 
        function(device)
        {
            if(ownerflug == 1 && device.advertising.match(/close/i)){
                boxlist.push(device.name);
            }
            if(ownerflug == 2 && device.advertising.match(/open/i)){
                boxlist.push(device.name);
            }
            //TODO idiom pattern?
            // boxlist.push = ownerflug == 1 && device.advertising.match(/close/i) && device.name ||
            //                ownerflug == 2 && device.advertising.match(/close/i) && device.name;
        },
        function(onError)
        {
            console.log("ble.scan error. msg:" + onError);
        }
    );
    //Wait for 5sec and callback.
    setTimeout(function(){
        if(onError){
            callback(null, onError);
        } else {
            callback(boxlist, null);
        }
    },5000);
};

//----------------------------------------------------------------
//connect to locking service, transfer of ownership
// args:    deviceId...     BLE device id (e.g. 00:ab:ef:34:21:11 )
//          ownAccount...   OwnAccount interface object.
//          transNunber...  Owner number of the transfer destination.(string)
//          callback...     resolve or reject
var lockingBoxTransfer = function lockingBoxTransfer(deviceId, ownAccount, transNunber, callback){
    //parameter check.
    if(typeof(deviceId) === 'undefined' || deviceId ==''){
        throw new Error(debugMsg() + "deviceId is undefined")};
    if(typeof(ownAccount) === 'undefined'){
        throw new Error(debugMsg() + "ownAccount is undefined")};
    if(typeof(transNunber) === 'undefined' || transNunber ==''){
        throw new Error(debugMsg() + "transNunber is undefined")};
    
    var okStatus = new Array();
    var okNotify = new Array();
    var ngStatus = new Array();
    var notifyDataValue;
    var onSuccess = function(){
        okStatus.push("OK");
    };
    var onSuccessNotify = function(){
        okNotify.push("OK");
    };
    var onError = function(reject){
        if(typeof reject === 'object'){
            console.warn(debugMsg() + "lockingBoxTransfer connect error, " + reject.errorMessage + " : " + reject.name + " : " + name.id );        
        } else {
            console.warn(debugMsg() + "lockingBoxTransfer error, " + reject);        
        }
        ngStatus.push(reject);
    };
   //ble.connect resolve callback function.
    var onConnect = function(){
        console.log(debugMsg() + "lockingBoxTransfer ble connected.");
        //ble.notify
        //ble.write
        ble.write(deviceId, UUID.KeylockService, UUID.LockBoxCharacteristic,
                stringToBytes(transNunber), onSuccess, onError);
        ble.write(deviceId, UUID.KeylockService, UUID.LockComsign1Characteristic, 
                stringToBytes(ownAccount.ownSign.substr(0,20)), onSuccess, onError);
        ble.write(deviceId, UUID.KeylockService, UUID.LockComsign2Characteristic,
                stringToBytes(ownAccount.ownSign.substr(20,20)), onSuccess, onError);
        ble.write(deviceId, UUID.KeylockService, UUID.LockComsign3Characteristic,
                stringToBytes(ownAccount.ownSign.substr(40,20)), onSuccess, onError);
        ble.write(deviceId, UUID.KeylockService, UUID.LockComsign4Characteristic,
                stringToBytes(ownAccount.ownSign.substr(60,20)), onSuccess, onError);
        ble.startNotification(deviceId, UUID.KeylockService, UUID.LockResultCharacteristic, 
                function(buffer){
                    onSuccess();
                    notifyDataValue = bytesToString(buffer);
                }, onError);
    };
    //ble.connect
    ble.connect(deviceId, onConnect, onError);
    //TODO promise.allに書き換える
    var intervalID = setInterval(function(){
        //TODO undefinedは"in ?" に書き換え
        if((okStatus.length >=6 || ngStatus.length)!=0){
            console.log(debugMsg() + "lockingBoxTransfer ble writeing finished.");
            clearInterval(intervalID);
            if(ngStatus.length!=0){
                callback(null, ngStatus);
            } else {
                callback(notifyDataValue, null);
            }
            ble.stopNotification(deviceId, UUID.KeylockService, UUID.LockResultCharacteristic, 
                    function(success){console.log(debugMsg() + "lockingBoxTransfer ble stopNotify " + success)}, 
                    function(failure){console.warn(debugMsg() + "lockingBoxTransfer ble stopNotify " + failure)});
            ble.disconnect(deviceId, function(success){console.log(debugMsg() + "lockingBoxTransfer ble Disconnect " + success)}, 
                    function(failure){console.warn(debugMsg() + "lockingBoxTransfer ble Disconnect " + failure)});
        }
    },500);
};

//connect to unlock service, transfer of ownership.
var unlockBoxTransfer = function unlockBoxTransfer(deviceId, ownAccount, transNunber, callback){
//    lockingBoxTransferと同じ処理
};
//----------------------------------------------------------------
//check the state of the box
var checkStateBox = function checkStateBox(deviceId, callback){
    var ngStatus = new Array();
    var onError = function(reject){
        console.log("ERROR: " + reject);
        ngStatus.push(reject);
    };
    var onData = function(buffer){
        onData.NotifyData = bytesToString(buffer);
    };

    var onConnect = function(){
        //ble.read
        ble.read(deviceId, UUID.KeyinfoService, UUID.KeyStatusCharacteristic, onData, onError);
    };
    //ble.connect
    ble.connect(deviceId, onConnect, onError);
    //TODO intervalで待つ必要なし
    var intervalID = setInterval(function(){
        //TODO undefinedは"in ?" に書き換え
        if((ngStatus.length)!=0 || typeof(onData.NotifyData) !== 'undefined'){
            console.log("checkstate end.");
            clearInterval(intervalID);
            if(ngStatus.length!=0){
                callback(null, ngStatus);
            } else {
                callback(onData.NotifyData, null);
            }
            ble.disconnect(deviceId, function(){
                console.log("Disconnect ok!")}, function(){
                    console.log("Disconnect error!")});
        }
    },500);
};
//----------------------------------------------------------------
//Get box address
var getBoxAddress = function getBoxAddress(deviceId, callback){
    //checkStateBox と同じようなロジック
};

//----------------------------------------------------------------
// ASCII only
function stringToBytes(string) {
   var array = new Uint8Array(string.length);
   for (var i = 0, l = string.length; i < l; i++) {
       array[i] = string.charCodeAt(i);
    }
    return array.buffer;
}

// ASCII only
function bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
}
// debug timestamp
function debugMsg(){
    return new Date().toLocaleTimeString() + " ->";
}