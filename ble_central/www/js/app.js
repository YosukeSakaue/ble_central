function account() {
    console.log("account()");
    var tpl = templates['account'];

    gmoAccount.getBalance(function(err, balance) {
        var account = {
            address: gmoAccount.getAddress(),
            balance: balance
        };

        var html = tpl.render(account);
        $('#content').html(html);
    });
}

function address_index() {
    console.log("address_index()");
    var tpl = templates['address_index'];
    var users = JSON.parse(localStorage.getItem('users'));
    var html = tpl.render({
        users: users
    });

    $('#content').html(html);
}

function address_add() {
    console.log("address_add()");
    var tpl = templates['address_add'];
    var html = tpl.render();

    $('#content').html(html);
}

function addAddress() {
    console.log("addAddress()");
    var users = JSON.parse(localStorage.getItem('users'));
    if (users === null) {
        users = [];
    }
    users.push({name: $("#inputName").val(), address: $("#inputAddress").val()});
    localStorage.setItem('users', JSON.stringify(users));

    address_index();
}

function removeAddress(address) {
    console.log("removeAddress()");
    var users = JSON.parse(localStorage.getItem('users'));
    if (users === null) {
        address_index();
    }
    var removedUsers = users.filter(function(item, index) {
        if (item.address !== address) return true;
    })
    localStorage.setItem('users', JSON.stringify(removedUsers));

    address_index();
}

function buy() {
    var tpl = templates['buy'];

    contract.callGetEvent(PurchaseFactoryContract.address, PurchaseFactoryContract.abi, 'PurchaseCreationEvent', {}, null, function(err, events) {
        if (err) throw err;

        const allFilesInfo = [];

        if (events.length === 0) {
            var html = tpl.render({
                files: []
            });

            $('#content').html(html);
        }

        events.forEach(function(event, i, events) {
            fileObject.getFileInfo(password, event.args.fileObject, function(err, info) {
                if (err) throw err;

                var purchase = event.args.purchase;

                contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'isActive', [], function(err, isActive) {
                    if (err) throw err;

                    isActive = isActive[0];

                    contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'getPrice', [], function(err, price) {
                        if (err) throw err;

                        allFilesInfo.push({name: info.name, category: info.metadata, address: info.address, purchase: purchase, isActive: isActive, price: price});

                        if (allFilesInfo.length === events.length) {

                            var html = tpl.render({
                                files: allFilesInfo
                            });

                            $('#content').html(html);
                        }
                    });
                });
            });            
        });
    });
}

function makePurchase(purchase, price) {
    console.log("Buying ... " + purchase + ' for ' + price);

    contract.callTransactionMethod(password, purchase, PurchaseContract.abi, 'makePurchase', [], price, function(err, res) {
        if (err) throw err;

        var tpl = templates['bought'];
        var html = tpl.render();
        $('#content').html(html);    
    });
}

function demoRequestSendBalance() {
    var demo = new Demo(gmoAccount);
    demo.requestSendBalance();
}
function deposit() {
    var tpl = templates['deposit'];
    var html = tpl.render();
    $('#content').html(html);
}

function depositValue() {
    var value = $("#deposit-value").val();
    console.log("deposit value: " + value);

    var gmoDeposit = new Deposit(gmoAccount);
    gmoDeposit.deposit(password, value, function(err, res) {
        if (err) {
            console.log(err);
        } else {
            console.log('deposit response: ' + res);
            deposited(value);
        }
    });
}

function changeDepositValue(value) {
    console.log("change deposit value: " + value);
    $("#deposit-value").val(value);
}
function deposited(value, total) {
    var tpl = templates['deposited'];
    var html = tpl.render({value: value, total: total});
    $('#content').html(html);
}
function download() {
    console.log("download()");
    var tpl = templates['download'];

    var fileObject = new FileObject(gmoAccount);
    fileObject.getAllReadableFileInfo(password, function(err, res) {
        console.log(res);
        var html = tpl.render({
            files: res
        });

        $('#content').html(html);
    });
}

function downloadFile(address, filename) {
    console.log("downloadFile()");
    var fileObject = new FileObject(gmoAccount);
    fileObject.download(password, address, filename);
}

function header() {
    console.log("header()");
    var gmoDeposit = new Deposit(gmoAccount);
    gmoDeposit.getDepositBalance(password, function(err, res) {
        if (err) {
            console.log(err);
        } else {
            console.log(res);
            var tpl = templates['header'];
            var html = tpl.render({ value: res });
            $('#header').html(html);
        }
    });
}

function reload() {
    console.log('reload()');
    header();
}

function home() {
    header();

    var tpl = templates['home'];
    var html = tpl.render({files: []});

    $('#content').html(html);

    var fileObject = new FileObject(gmoAccount);
    fileObject.getAllFilesInfo(password, function(err, res) {
        var tpl = templates['home'];

        var html = tpl.render({
            files: res
        });

        $('#content').html(html);
    });
}

var password;
var gmoAccount = new Account();
var contract = new Contract(gmoAccount);
var fileObject = new FileObject(gmoAccount);

function index() {
    console.log("index()");
    if (gmoAccount.getAddress()) {
        if (password) {
            home();
        } else {
            login();
        }
    } else {
        register();
    }
}

function login() {
    console.log("login()");
    var tpl = templates['login'];
    var html = tpl.render();
    $('#content').html(html);
}

function loginAccount() {
    password = $("#loginPassword").val();

    gmoAccount.login(password, function(err, address) {
        if (err) {
            $(".errMsg").html(err);
        } else {
            home();
        }
    });
}

function menu() {
    $('#page-title').hide();
    var tpl = templates['menu'];

    var menus = [{
        name: 'ホーム',
        action: 'home()'
    }, {
        name: 'アドレス一覧',
        action: 'address_index()'
    }, {
        name: 'ダウンロード一覧',
        action: 'download()'
    }, {
        name: '販売一覧',
        action: 'sell()'
    }, {
        name: '購入一覧',
        action: 'buy()'
    }, {
        name: 'アカウント',
        action: 'account()'
    }, {
        name: 'デポジット',
        action: 'deposit()'
    }];

    var html = tpl.render({
        menus: menus
    });

    $('#content').html(html);
}

function register() {
    console.log("register()");
    var tpl = templates['register'];
    var html = tpl.render();
    $('#content').html(html);
}

function createAccount() {
    var inputPassword = $("#inputPassword").val();
    var confirmPassword = $("#inputConfirmPassword").val();
    var errMsg = $(".errMsg");

    if (!inputPassword || !confirmPassword) {
        $(".errMsg").html('パスワードを入力してください。');
        return;
    }

    if (inputPassword !== confirmPassword) {
        $(".errMsg").html('Not match password');
        return;
    }
    password = inputPassword;

    gmoAccount.register(password, function(err, address) {
        if (err) {
            $(".errMsg").html(err);
        } else {
            home();
        }
    });
}


function selling(fileObjectAddress) {
    fileObject.getFileInfo(password, fileObjectAddress, function(err, info) {
        if (err) throw err;

        contract.callConstantMethod(password, PurchaseFactoryContract.address, PurchaseFactoryContract.abi, 'getPurchases', [], function(err, purchases) {
            if (err) throw err;

            purchases = purchases[0];
            var purchase = null;
            var counter = -1;
            
            if (purchases.length === 0) {
                var file = {
                    name: info.name,
                    category: info.metadata,
                    address: fileObjectAddress
                };

                var tpl = templates['selling'];
                var html = tpl.render(file);
                $('#content').html(html);    
            }

            for (var i=0; i<purchases.length; i++) {
                contract.callConstantMethod(password, purchases[i], PurchaseContract.abi, 'getTargetObject', [], function(err, targetObject) {
                    if (err) throw err;
                    counter++;

                    if (!purchase && targetObject[0] === fileObjectAddress) {
                        const purchase = purchases[counter];

                        contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'getBeneficiaries', [], function(err, beneficiaryData) {
                            if (err) throw err;

                            const users = JSON.parse(localStorage.getItem('users'));

                            const beneficiaries = beneficiaryData[0].map(function(address, i) {
                                var user = users.filter(function(user) { 
                                  return user.address === address 
                                })[0];
                                const name = user ? user.name : '';

                                return ({address:address, name: name, rate: beneficiaryData[1][i]});
                            });

                            contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'getPrice', [], function(err, price) {
                              if (err) throw err;

                              var file = {
                                  name: info.name,
                                  category: info.metadata,
                                  beneficiaries: beneficiaries,
                                  address: fileObjectAddress,
                                  purchase: purchase,
                                  price: price
                              };

                              var tpl = templates['selling_purchase'];
                              var html = tpl.render(file);
                              $('#content').html(html);
                            });
                        });
                    } else if (!purchase &&  counter === purchases.length - 1) {
                        // We haven't found the purchase so display purchase entry screen
                        var file = {
                            name: info.name,
                            category: info.metadata,
                            address: fileObjectAddress
                        };

                        var tpl = templates['selling'];
                        var html = tpl.render(file);
                        $('#content').html(html);    
                    }
                });
            }
        });
    });
}

function createPurchase(fileObjectAddress) {
    const purchasePrice = $('#purchase-price').val();
    contract.callTransactionMethod(password, PurchaseFactoryContract.address, PurchaseFactoryContract.abi, 'createPurchase', [fileObjectAddress, purchasePrice], 0, function(err, res) {
        if (err) throw err;

        var tpl = templates['purchase_created'];
        var html = tpl.render();
        $('#content').html(html);
    });
}

function chooseBeneficiary(purchase, fileObjectAddress) {
    var tpl = templates['seller'];

    fileObject.getFileInfo(password, fileObjectAddress, function(err, info) {
        if (err) throw err;
        
        const users = JSON.parse(localStorage.getItem('users'));
        
        var file = {
          category: info.metadata,
          name: info.name,
          address: purchase,
          users: users
      };

      var html = tpl.render(file);
      $('#content').html(html);
    });
}

function addBeneficiary(purchase) {
    const seller = $('#seller-address').val();

    contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'getBeneficiaries', [], function(err, beneficiaries) {
        if (err) throw err;

        const b = beneficiaries[0];
        b.push(seller);
        const rate = Math.floor(100/b.length);
        const last = b.length - 1;
        var newRates = b.map(function(v, i) {
            if (i === last) {
                return 100 - (last * rate);
            } else {
                return rate;
            }
        });

        contract.callTransactionMethod(password, purchase, PurchaseContract.abi, 'setBeneficiaries', [b, newRates], 0, function(err, res) {
            if (err) throw err;

            var tpl = templates['sellers_set'];
            var html = tpl.render();
            $('#content').html(html);
        });
    });
}

function delBeneficiary(seller, purchase) {

    contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'getBeneficiaries', [], function(err, beneficiaries) {
        if (err) throw err;

        const b = beneficiaries[0].filter(function(v, i) {
          return v !== seller;
        });

        var newRates = [];
        if (b.length > 0) {
          const rate = Math.floor(100/b.length);
          const last = b.length - 1;
          newRates = b.map(function(v, i) {
              if (i === last) {
                  return 100 - (last * rate);
              } else {
                  return rate;
              }
          });
        }

        contract.callTransactionMethod(password, purchase, PurchaseContract.abi, 'setBeneficiaries', [b, newRates], 0, function(err, res) {
            if (err) throw err;

            var tpl = templates['sellers_set'];
            var html = tpl.render();
            $('#content').html(html);
        });
    });
}

function sell() {
    var tpl = templates['sell'];

    contract.callConstantMethod(password, PurchaseFactoryContract.address, PurchaseFactoryContract.abi, 'getPurchases', [], function(err, purchases) {
        if (err) throw err;

        purchases = purchases[0];
        const files = [];

        if (purchases.length === 0) {
            var html = tpl.render({
                files: []
            });

            $('#content').html(html);
        }

        purchases.forEach(function(purchase, index, purchases) {
            contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'getTargetObject', [], function(err, fileObjectAddress) {
                if (err) throw err;

                fileObject.getFileInfo(password, fileObjectAddress[0], function(err, info) {
                    if (err) {
                        console.error(err.response.body.message);
                        throw err;
                    }

                    contract.callConstantMethod(password, purchase, PurchaseContract.abi, 'isActive', [], function(err, isActive) {
                        if (err) throw err;
                        
                        isActive = isActive[0];
                        files.push({name: info.name, category: info.metadata, purchaseAddress: purchase, is_active: isActive, not_active: !isActive, fileObjectAddress: info.address});
                        if (files.length === purchases.length) {
                            var html = tpl.render({
                                files: files
                            });

                            $('#content').html(html);
                        }
                    });
                });
            });
        });
    });
}

function setSaleStatus(address, action, fileObjectAddress) {
    console.log("selling: " + address + " " + action);

    contract.callTransactionMethod(password, address, PurchaseContract.abi, action, [], 0, function(err, res) {
        if (err) throw err;

        var tpl = templates['sale_set'];
        var html = tpl.render();
        $('#content').html(html);

        if (action === 'start') {
            contract.callTransactionMethod(password, fileObjectAddress, ObjectContract.abi, 'addAccessController', [address], 0, function(err, res) {
                if (err) throw err;  

                console.log('added purchase instance ' + address + ' to access controller list for ' + fileObjectAddress);
            }, 1);
        } else {
            contract.callTransactionMethod(password, fileObjectAddress, ObjectContract.abi, 'removeAccessController', [address], 0, function(err, res) {
                if (err) throw err;  

                console.log('removed purchase instance ' + address + ' from access controller list for ' + fileObjectAddress);
            }, 1);
        }
    });
}

const PurchaseFactoryContract = {
    "abi": [
      {
        "constant": false,
        "inputs": [
          {
            "name": "purchaseTarget",
            "type": "address"
          },
          {
            "name": "purchasePrice",
            "type": "uint256"
          }
        ],
        "name": "createPurchase",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "getPurchases",
        "outputs": [
          {
            "name": "",
            "type": "address[]"
          }
        ],
        "type": "function"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "name": "purchase",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "fileObject",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "creator",
            "type": "address"
          }
        ],
        "name": "PurchaseCreationEvent",
        "type": "event"
      }
    ],
    "updated_at": 1472727958310,
    "address": "0xf8e78296f8995b58b1e32ca7bb6bd3ba0aeca65a"
};

const PurchaseContract = {
    "abi": [
      {
        "constant": false,
        "inputs": [
          {
            "name": "addrs",
            "type": "address[]"
          },
          {
            "name": "rates",
            "type": "uint256[]"
          }
        ],
        "name": "setBeneficiaries",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "stop",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "getTargetObject",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "isActive",
        "outputs": [
          {
            "name": "",
            "type": "bool"
          }
        ],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "target",
            "type": "address"
          }
        ],
        "name": "setTargetObject",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "makePurchase",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "getBeneficiaries",
        "outputs": [
          {
            "name": "addrs",
            "type": "address[]"
          },
          {
            "name": "rates",
            "type": "uint256[]"
          }
        ],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "p",
            "type": "uint256"
          }
        ],
        "name": "setPrice",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "getPrice",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "start",
        "outputs": [],
        "type": "function"
      },
      {
        "inputs": [
          {
            "name": "sender",
            "type": "address"
          },
          {
            "name": "purchaseTarget",
            "type": "address"
          },
          {
            "name": "purchasePrice",
            "type": "uint256"
          }
        ],
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "name": "payer",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "payee",
            "type": "address"
          },
          {
            "indexed": false,
            "name": "amount",
            "type": "uint256"
          }
        ],
        "name": "PaymentEvent",
        "type": "event"
      }
    ],
    "updated_at": 1472804132773
};

const ObjectContract = {
    "abi": [
      {
        "constant": false,
        "inputs": [
          {
            "name": "hash",
            "type": "bytes32"
          }
        ],
        "name": "setDataHashByGmo",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "name": "sender",
            "type": "address"
          }
        ],
        "name": "isWriter",
        "outputs": [
          {
            "name": "",
            "type": "bool"
          }
        ],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "addr",
            "type": "address"
          }
        ],
        "name": "removeAccessController",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "addr",
            "type": "address"
          }
        ],
        "name": "addAccessController",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "name": "sender",
            "type": "address"
          }
        ],
        "name": "isReader",
        "outputs": [
          {
            "name": "",
            "type": "bool"
          }
        ],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "getOwner",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "getDataHash",
        "outputs": [
          {
            "name": "hash",
            "type": "bytes32"
          }
        ],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "hash",
            "type": "bytes32"
          }
        ],
        "name": "setDataHashByOwner",
        "outputs": [],
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "name": "sender",
            "type": "address"
          },
          {
            "name": "gmo",
            "type": "address"
          }
        ],
        "name": "init",
        "outputs": [],
        "type": "function"
      },
      {
        "inputs": [],
        "type": "constructor"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "name": "sender",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "hash",
            "type": "bytes32"
          }
        ],
        "name": "DataHashEvent",
        "type": "event"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "name": "creator",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "owner",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "gmo",
            "type": "address"
          }
        ],
        "name": "InitEvent",
        "type": "event"
      }
    ],
    "updated_at": 1472727958296
};

function share(fileObjectAddress) {
    var tpl = templates['share'];
    var fileObject = new FileObject(gmoAccount);
    fileObject.getFileInfo(password, fileObjectAddress, function(err, info) {
        if (err) throw err;

        fileObject.getReaders(fileObjectAddress, function(err, readers) {
            if (err) throw err;

            fileObject.getWriters(fileObjectAddress, function(err, writers) {

                const users = JSON.parse(localStorage.getItem('users'));

                readers = readers.map(function(reader) {
                    var user = users.filter(function(user) { return user.address === reader.reader })[0];
                    const name = user ? user.name : '';

                    return ({reader:reader.reader, name: name});
                });

                writers = writers.map(function(writer) {
                    var user = users.filter(function(user) { return user.address === writer.writer })[0];
                    const name = user ? user.name : '';

                    return ({writer:writer.writer, name: name});
                });

                var file = {
                    name: info.name,
                    category: info.metadata,
                    writer: writers,
                    reader: readers,
                    address: fileObjectAddress
                };

                var html = tpl.render(file);
                $('#content').html(html);
            });
        });
    });
}

function addWriter(fileObjectAddress) {
    var tpl = templates['writer'];
    var fileObject = new FileObject(gmoAccount);
    fileObject.getFileInfo(password, fileObjectAddress, function(err, info) {
        if (err) throw err;

        const users = JSON.parse(localStorage.getItem('users'));

        var file = {
            address: fileObjectAddress,
            name: info.name,
            users: users
        };

        var html = tpl.render(file);
        $('#content').html(html);
    });
}

function submitWriter() {
    const fileObjectAddress = $('#file-object-address').val();
    const writer = $('#writer-address').val();
    var fileObject = new FileObject(gmoAccount);
    fileObject.addWriter(password, writer, fileObjectAddress, function(err, res) {
        if (err) throw err;

        var tpl = templates['writer_added'];
        var html = tpl.render();
        $('#content').html(html);
    });
}

function delWriter(writer) {
    const fileObjectAddress = $('#file-object-address').val();
    var fileObject = new FileObject(gmoAccount);
    fileObject.deleteWriter(password, writer, fileObjectAddress, function(err, info) {
        if (err) throw err;

        var tpl = templates['writer_deleted'];
        var html = tpl.render();
        $('#content').html(html);
    });
}

function addReader(fileObjectAddress) {
    var tpl = templates['reader'];
    var fileObject = new FileObject(gmoAccount);
    fileObject.getFileInfo(password, fileObjectAddress, function(err, info) {
        if (err) throw err;

        const users = JSON.parse(localStorage.getItem('users'));

        var file = {
            address: fileObjectAddress,
            name: info.name,
            users: users
        };

        var html = tpl.render(file);
        $('#content').html(html);
    });
}

function submitReader() {
    const fileObjectAddress = $('#file-object-address').val();
    const reader = $('#reader-address').val();
    var fileObject = new FileObject(gmoAccount);
    fileObject.addReader(password, reader, fileObjectAddress, function(err, res) {
        if (err) throw err;

        var tpl = templates['reader_added'];
        var html = tpl.render();
        $('#content').html(html);
    });
}

function delReader(reader) {
    const fileObjectAddress = $('#file-object-address').val();
    var fileObject = new FileObject(gmoAccount);
    fileObject.deleteReader(password, writer, fileObjectAddress, function(err, info) {
        if (err) throw err;

        var tpl = templates['reader_deleted'];
        var html = tpl.render();
        $('#content').html(html);
    });
}

var tpl = templates['test'];
var html = tpl.render({
    id: 1,
    name: 'Jack',
    age: 20,
    gender: 'male',
    phone: '09012345678'
});

function change() {
     $('#btn').html('change');
}
console.log($('#btn'));
console.log($('#content'));
$('#content').html(html);

function upload() {
    var tpl = templates['upload'];
    var html = tpl.render();
    $('#content').html(html);
}

function submit() {
    console.log("Uploading file...");

    const file = $('#file')[0].files[0];
    const metadata = $('#metadata').val();
    var fileObject = new FileObject(gmoAccount);
    metadataHash = fileObject.hash(metadata);
    console.log('metadataHash: ' + metadataHash);

    fileObject.createFileObject(password, file, metadata, function(err, result) {
        if (err) {
            throw err;
        }

        console.log('fileObjectAddress: ' + result);
    });

    uploaded();
}

function getInfo() {
    var file = $('#file')[0].files[0];
    console.log(file);

    // Set file name & type
    $('#filename').text(file.name);
    $('#type').text(file.type);

    // Set file size
    var iSize = file.size / 1024;
    iSize = (Math.round((iSize / 1024) * 100) / 100);
    $('#size').text(iSize + " MB");
}

function uploaded() {
    var tpl = templates['uploaded'];
    var html = tpl.render({});
    $('#content').html(html);
}
